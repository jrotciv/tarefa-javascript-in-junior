const booksByCategory = [
    {
    category: "Riqueza",
    books: [
    {
    title: "Os segredos da mente milionária",
    author: "T. Harv Eker",
    },
    {
    title: "O homem mais rico da Babilônia",
    author: "George S. Clason",
    },
    {
    title: "Pai rico, pai pobre",
    author: "Robert T. Kiyosaki e Sharon L. Lechter",
    },
    ],
    },
    {
    category: "Inteligência Emocional",
    books: [
    {
    title: "Você é Insubstituível",
    author: "Augusto Cury",
    },
    {
    title: "Ansiedade – Como enfrentar o mal do século",
    author: "Augusto Cury",
    },
    {
    title: "Os 7 hábitos das pessoas altamente eficazes",
    author: "Stephen R. Covey"
    }
    ]
    }
    ];

var autoresVerificados = []
var posicaoVerificacao = 0

booksByCategory.forEach((categoria) => {
    for (book of categoria.books){
        //Condiçao para evitar repetição de autor na lista
        if (autoresVerificados.indexOf(book.author) < 0){
            autoresVerificados[posicaoVerificacao] = book.author
            posicaoVerificacao += 1
        }
    }
})

console.log(`Há ${autoresVerificados.length} autores na array`)