const booksByCategory = [
    {
    category: "Riqueza",
    books: [
    {
    title: "Os segredos da mente milionária",
    author: "T. Harv Eker",
    },
    {
    title: "O homem mais rico da Babilônia",
    author: "George S. Clason",
    },
    {
    title: "Pai rico, pai pobre",
    author: "Robert T. Kiyosaki e Sharon L. Lechter",
    },
    ],
    },
    {
    category: "Inteligência Emocional",
    books: [
    {
    title: "Você é Insubstituível",
    author: "Augusto Cury",
    },
    {
    title: "Ansiedade – Como enfrentar o mal do século",
    author: "Augusto Cury",
    },
    {
    title: "Os 7 hábitos das pessoas altamente eficazes",
    author: "Stephen R. Covey"
    }
    ]
    }
    ];

const autor = prompt('De que autor deseje ver os livros?')

console.log(`Livro(s) do autor ${autor} na array:`)

booksByCategory.forEach((categoria) => {
    for (book of categoria.books){

        if (book.author == autor){
            console.log(book.title)
        }
    }
})