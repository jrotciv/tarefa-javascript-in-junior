function multiplicarMatriz(matrizA, matrizB){
    let matrizResultante = []

    for (let linhaA = 0; linhaA < matrizA.length; linhaA++) {
        matrizResultante[linhaA] = []

        for (let colunaB = 0; colunaB < matrizB[0].length; colunaB++){
            let somaDosProdutos = 0;

            for (let colunaA = 0; colunaA < matrizA[0].length; colunaA++){
                somaDosProdutos += matrizA[linhaA][colunaA] * matrizB[colunaA][colunaB]
            }

            matrizResultante[linhaA][colunaB] = somaDosProdutos
        }
    }

    return matrizResultante
}

var matrizA = [[[2],[-1]], [2,0]]
var matrizB = [[2,3], [-2,1]]

if (matrizA[0].length == matrizB.length){
    console.log(multiplicarMatriz(matrizA, matrizB))
}
else{
    console.log('Para multiplicar é preciso que o número de colunas da primeira seja igual oa número de linhas da segunda!')
}