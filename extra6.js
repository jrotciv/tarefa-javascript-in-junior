function mostrarSaldo(receitasDespesas){
    let valorSaldo = 0

    receitasDespesas.receitas.forEach((receita) => {
        valorSaldo += receita
    });

    receitasDespesas.despesas.forEach((despesa) => {
        valorSaldo -= despesa
    })

    if (valorSaldo > 0){
        console.log(`A familia está com saldo positivo de R$${valorSaldo}`)
    }
    else if (valorSaldo < 0){
        console.log(`A familia está com saldo negativo de R$${valorSaldo}`)
    }
    else{
        console.log(`A familia está com saldo nulo`)
    }
}

const receitasDespesas = {
    receitas: [5, 5, 7],
    despesas: [2, 500.5]  
}

mostrarSaldo(receitasDespesas)