function potencia(base, expoente){
    if (expoente == 0){
        return 1
    }
    else{
        return base * potencia(base, expoente - 1)
    }   
}

var base = 3
var expoente = 5

console.log(potencia(base, expoente))