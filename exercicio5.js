const valores = []
var inteiroMaximo = 100 //Define valor maximo desejado

for (let quantidade = 0; quantidade < 5; quantidade++){
    valores[quantidade] = parseInt(Math.random() * inteiroMaximo) //Adiciona um valor inteiro, entre o intervalo 0 e inteiroMaximo, na lista
}

console.log('Valores:', valores)

valores.forEach((valor) => {
    if (valor % 15 == 0){
        console.log(valor + ': fizzbuzz')
    }
    else if (valor % 5 == 0){
        console.log(valor + ': buzz')
    }
    else if (valor % 3 == 0){
        console.log(valor + ': fizz')
    }
})

