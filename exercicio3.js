function verificarAprovacao(nota1, nota2, nota3){
    let media = (nota1 + nota2 + nota3)/3

    if (media >= 6) {
        return 'Aprovado'
    }
    else{
        return 'Reprovado'
    }
}

var nota1 = parseFloat(prompt('Primeira nota do aluno:'))
var nota2 = parseFloat(prompt('Segunda nota do aluno:'))
var nota3 = parseFloat(prompt('Terceira nota do aluno:'))

console.log(verificarAprovacao(nota1, nota2, nota3))