function ordenar(valorAtual, valorSucessor){
    if (valorAtual > valorSucessor){
        return 1
      }
      if (valorAtual < valorSucessor) {
        return -1
      }
      return 0
}

const valores = [5, 74, 5, 60, 10]
const valoresOrdenados = valores.slice().sort(ordenar)

console.log(`Valores ordenados: ${valoresOrdenados}`)
console.log(`\nValores na ordem inicial: ${valores}`)


